//
//  main.m
//  Cert
//
//  Created by Digisa Digisa on 10/01/12.
//  Copyright (c) 2012 Digisa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
