//
//  AppDelegate.m
//  Cert
//
//  Created by Ricardo, Pedro Digisa on 10/01/12.
//  Copyright (c) 2012 Digisa. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;

- (void)dealloc{
    [super dealloc];
}
	
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {    

}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)theApplication hasVisibleWindows:(BOOL)flag{

    if (!_window.isVisible) {
        [_window makeKeyAndOrderFront:nil];

        return YES;
    }
    
    
    return NO;
    
}




- (IBAction)TouchCert:(id)sender{//pega origem arquivo-cer.p12
                         
    NSArray * files = nil;
    NSOpenPanel *  oPanel = [NSOpenPanel openPanel];
    
    [oPanel setCanChooseDirectories:NO];
    [oPanel setCanChooseFiles:YES];
    [oPanel setCanCreateDirectories:YES];
    [oPanel setAllowsMultipleSelection:NO];
    [oPanel setAlphaValue:0.95];
    [oPanel setTitle:@"Select the [file]-cer.p12"];
    
   if([oPanel runModal] == NSOKButton){
       
       files = [oPanel URLs];
              
       if(files) {
           NSURL *url = [files objectAtIndex:0];
           NSLog(@"%@", [url path]);

       }
   }

}


- (IBAction)TouchKey:(id)sender{
    
    NSArray * files = nil;    
    //NSArray * fileTypes = [NSArray arrayWithObjects:@"p12", nil];
    NSOpenPanel *  oPanel = [NSOpenPanel openPanel];
    
    [oPanel setCanChooseDirectories:NO];
    [oPanel setCanChooseFiles:YES];
    [oPanel setCanCreateDirectories:YES];
    [oPanel setAllowsMultipleSelection:NO];
    [oPanel setAlphaValue:0.95];
    [oPanel setTitle:@"Select the [file]-key.p12"];
        
    if([oPanel runModal] == NSOKButton){
        files = [oPanel URLs];
        
        if(files) {
            NSURL *url = [files objectAtIndex:0];
            NSLog(@"%@", [url path]);            
            [origemKey setStringValue:[url path]];
        }
    }

}

- (IBAction)TouchDestino:(id)sender{
    
    NSArray * files = nil;
    NSOpenPanel *  oPanel = [NSOpenPanel openPanel];
    
    [oPanel setCanChooseDirectories:YES];
    [oPanel setCanChooseFiles:NO];
    [oPanel setCanCreateDirectories:YES];
    [oPanel setAllowsMultipleSelection:NO];
    [oPanel setAlphaValue:0.95];
    [oPanel setTitle:@"Select the output path"];
    
    if([oPanel runModal] == NSOKButton){
        files = [oPanel URLs];
        
        if(files) {
            NSURL *url = [files objectAtIndex:0];
            NSLog(@"%@", [url path]);            
            [destinoPem setStringValue:[url path]];
        }
    }

}

- (IBAction)TouchGerarPEM:(id)sender{
    
    if([[origemKey stringValue] length] > 0 && [[destinoPem stringValue] length] > 0 && [[passWord stringValue] length] > 0)
    {
        

        if(![[NSFileManager defaultManager] fileExistsAtPath:[origemKey stringValue]])
        {
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:[NSString stringWithFormat:@"The following key is incorrect:\n%@", [origemKey stringValue]]];
            [alert runModal];
            [alert release];
            return;
        }
        
        if(![[NSFileManager defaultManager] fileExistsAtPath:[destinoPem stringValue]])
        {
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:[NSString stringWithFormat:@"The output path is incorrect:\n%@", [destinoPem stringValue]]];
            [alert runModal];
            [alert release];
        }
    }
    else
    {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"All fields are required!"];
        [alert runModal];
        [alert release];
        return;
    }
    
    //openssl pkcs12 -in NOME.p12 -out NOME.pem -nodes
    
    NSPipe *inputPipe = [NSPipe pipe];

    NSTask *task = [[NSTask alloc] init];
    task.launchPath = @"/usr/bin/openssl";
    task.arguments  = @[@"pkcs12",@"-in",[origemKey stringValue],@"-out",[NSString stringWithFormat:@"%@/%@.pem",[destinoPem stringValue],[nomeFinal stringValue]], @"-nodes"];
    [task setStandardInput:inputPipe];
    [task launch];
    
    NSString* passText = [NSString  stringWithFormat:@"%@\n",[passWord stringValue]];
    
    [[inputPipe fileHandleForWriting] writeData:[passText dataUsingEncoding:NSUTF8StringEncoding]];
    [task waitUntilExit];
    [task release];

    
    NSAlert *alert = [[NSAlert alloc] init];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@.pem",[destinoPem stringValue],[nomeFinal stringValue]]])
    {

        if([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/apns-dev-cert.pem",[destinoPem stringValue]]])
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/apns-dev-cert.pem",[destinoPem stringValue]] error:nil];

        if([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/apns-dev-key.pem",[destinoPem stringValue]]])
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/apns-dev-key.pem",[destinoPem stringValue]] error:nil];

        if([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/apns-dev-key-noenc.pem",[destinoPem stringValue]]])
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/apns-dev-key-noenc.pem",[destinoPem stringValue]] error:nil];

        [alert setMessageText:@".PEM File generated with success!"];
        [origemKey setStringValue:@""];
        [destinoPem setStringValue:@""];
        [passWord setStringValue:@""];
        [nomeFinal setStringValue:@""];
    }
    else
    {
        [alert setMessageText:@"There was an error trying to generate the .PEM file!"];
    }
    
    [alert runModal];
    [alert release];
}


@end
